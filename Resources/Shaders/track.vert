#version 330 core

// Positions/Coordinates
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;
layout (location = 3) in vec2 aTex;
layout (location = 4) in mat4 instanceMatrix;

// Outputs the current position for the Fragment Shader
out vec3 crntPos;
out vec3 Normal;
out vec2 texCoord;
out vec4 fragPosLightSpace;
// Imports the camera matrix
uniform mat4 camMatrix;
uniform mat4 lightSpaceMatrix;

void main()
{
	// calculates current position
	crntPos = vec3(instanceMatrix * vec4(aPos, 1.0f));
	Normal = mat3(transpose(inverse(instanceMatrix))) * aNormal;
	texCoord = aTex;
	fragPosLightSpace = lightSpaceMatrix * vec4(crntPos, 1.0);

	// Outputs the positions/coordinates of all vertices
	gl_Position = camMatrix * vec4(crntPos, 1.0);
}