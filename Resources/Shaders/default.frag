#version 330 core

// Outputs colors in RGBA
out vec4 FragColor;

// Imports the current position from the Vertex Shader
in vec3 crntPos;
in vec3 Normal;
in vec2 texCoord;
in vec4 fragPosLightSpace;

// Inputs the color from the Vertex Shader
uniform vec3 camPos;
uniform vec3 lightPos;
uniform sampler2D diffuse0;
uniform sampler2D shadowMap;

void main()
{
    vec3 normal = normalize(Normal);
    vec3 lightColor = vec3(1.0);
    // ambient
    vec3 ambient = 0.5 * lightColor;
    // diffuse
    vec3 lightDir = normalize(lightPos - crntPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
    // specular
    vec3 viewDir = normalize(camPos - crntPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;    

	// calculate shadow
	float shadow = 0.0f;
	// Sets lightCoords to cull space
	vec3 lightCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
	if(lightCoords.z <= 1.0f)
	{
		// Get from [-1, 1] range to [0, 1] range just like the shadow map
		lightCoords = (lightCoords + 1.0f) / 2.0f;
		float currentDepth = lightCoords.z;
		// Prevents shadow acne
		float bias = max(0.025f * (1.0f - dot(normal, lightDir)), 0.0005f);

		// Smoothens out the shadows
		int sampleRadius = 3;
		vec2 pixelSize = 1.0 / textureSize(shadowMap, 0);
		for(int y = -sampleRadius; y <= sampleRadius; y++)
		{
		    for(int x = -sampleRadius; x <= sampleRadius; x++)
		    {
			    float closestDepth = texture(shadowMap, lightCoords.xy + vec2(x, y) * pixelSize).r;
				if (currentDepth > closestDepth + bias)
					shadow += 1.0f;
		    }    
		}
		// Get average shadow
		shadow /= pow((sampleRadius * 2 + 1), 2);
	}

    vec3 color = texture(diffuse0, texCoord).rgb;
    vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;
    
    // FragColor = vec4(shadow, 0.5f, 0.5f, 1.0f);
	// FragColor = vec4(diffuse, 1.0f);
    // FragColor = texture(shadowMap, lightCoords.xy);
    FragColor = vec4(lighting, 1.0);
}