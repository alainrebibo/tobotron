#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 4) in mat4 instanceMatrix;

uniform mat4 lightSpaceMatrix;
uniform mat4 transformation;
uniform int instances;

void main()
{
	if (instances != 1)
	{
		gl_Position = lightSpaceMatrix * instanceMatrix * vec4(aPos, 1.0f);
	}
	else
	{
		gl_Position = lightSpaceMatrix * transformation * vec4(aPos, 1.0f);
	}
}