#version 330 core

// Positions/Coordinates and color
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec4 aColor;

// Outputs the current position for the Fragment Shader
out vec4 color;

// Imports the camera matrix
uniform mat4 camMatrix;

void main()
{
	// Outputs the positions/coordinates of all vertices
	gl_Position = camMatrix * vec4(aPos, 1.0);

	color = aColor;
}