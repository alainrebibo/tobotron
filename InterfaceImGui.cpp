#include "InterfaceImGui.h"

using namespace std;

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

InterfaceImGui::InterfaceImGui() 
{
	interface_speed = 0;
	m_currentLandscape = 0;
	m_trihedron = false;
	cameraTypes = { CameraType::FirstPerson };
}

void InterfaceImGui::startNewFrame()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void add_or_remove(vector<CameraType>& vec, CameraType cameraType)
{
	std::vector<CameraType>::iterator it;

	it = find(begin(vec), end(vec), cameraType);

	if (it != end(vec))
	{
		if (vec.size() <= 1) return;
		vec.erase(it);
	}
	else
	{
		vec.push_back(cameraType);
		std::sort(begin(vec), end(vec));
	}
}

/*
# C�mara

	Movimiento de c�mara: `LEFT SHIFT` + `Click LEFT` rat�n

	Modo carrera ON: `F3`

	Camara libre ON: `F4`

	Primera persona ON: `F5`

	Tercera Persona ON: `F6`

# Accesos rapidos

	Subir velocidad x10: `key UP` (flecha hacia arriba)

	Bajar velocidad x10: `key DOWN` (flecha hacia abajo)

	Subir velocidad: `key UP` +`LEFT SHIFT`

	Bajar velocidad: `key DOWN` + `LEFT SIFT`
*/
void InterfaceImGui::Inputs(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS)
	{
		this->setCameras({ CameraType::Race, CameraType::ThirdPerson });
	}

	if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS && firstF3)
	{
		add_or_remove(cameraTypes, CameraType::Race);
		firstF3 = false;
	}
	else if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_RELEASE) {
		firstF3 = true;
	}

	if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS && firstF4 && not firstPersonOn)
	{
		add_or_remove(cameraTypes, CameraType::Free);
		freeModeOn = !freeModeOn;
		firstF4 = false;
	}
	else if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_RELEASE) {
		firstF4 = true;
	}

	if (glfwGetKey(window, GLFW_KEY_F5) == GLFW_PRESS && firstF5 && not freeModeOn)
	{
		add_or_remove(cameraTypes, CameraType::FirstPerson);
		firstPersonOn = !firstPersonOn;
		firstF5 = false;
	}
	else if (glfwGetKey(window, GLFW_KEY_F5) == GLFW_RELEASE) {
		firstF5 = true;
	}

	if (glfwGetKey(window, GLFW_KEY_F6) == GLFW_PRESS && firstF6)
	{
		add_or_remove(cameraTypes, CameraType::ThirdPerson);
		firstF6 = false;
	}
	else if (glfwGetKey(window, GLFW_KEY_F6) == GLFW_RELEASE) {
		firstF6 = true;
	}

	if (glfwGetKey(window, GLFW_KEY_UP) && glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		speedUp(10.0f);
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) && glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		speedDown(10.0f);
	}
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		speedUp(1.0f);
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		speedDown(1.0f);
	}
}

void InterfaceImGui::setCameras(std::vector<CameraType> cameras)
{
	std::sort(begin(cameras), end(cameras));
	cameraTypes = cameras;
}
void InterfaceImGui::render()
{
	this->renderElements();
	this->renderIntoScreen();
}

void InterfaceImGui::renderElements()
{
	ImGui::Begin("Options");

	ImGui::Text("Performance: %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::SliderFloat("Speed", &interface_speed, SPEED_MAX, SPEED_MIN); // Edit 1 float using a slider from SPEED_MAX to SPEED_MIN

	m_speed = interface_speed <= 0 ? FLT_MAX : SPEED_MAX + (USER_SPEED_MIN * ((SPEED_MIN - interface_speed) / SPEED_MIN));
	ImGui::Text("");
	ImGui::Checkbox("Show Trihedrons", &m_trihedron);
	if (m_trihedron) {
		updateSpeed(0);
	}
	ImGui::Text("");
	if (ImGui::Button(m_landscape_choices[0])) // Buttons return true when clicked (NB: most widgets return true when edited/activated)
		m_currentLandscape = 0;
	if (ImGui::Button(m_landscape_choices[1]))
		m_currentLandscape = 1;
	if (ImGui::Button(m_landscape_choices[2]))
		m_currentLandscape = 2;
	if (ImGui::Button(m_landscape_choices[3]))
		m_currentLandscape = 3;

	ImGui::Text("Current landscape %s", m_landscape_choices[m_currentLandscape]);
	ImGui::Text("");

	if (ImGui::RadioButton("Race", &camaraSelected, 1))
		this->setCameras({ CameraType::Race });
	if (ImGui::RadioButton("First person", &camaraSelected, 2))
		this->setCameras({ CameraType::FirstPerson });
	if(ImGui::RadioButton("Third person", &camaraSelected, 3))
		this->setCameras({ CameraType::ThirdPerson});
	if(ImGui::RadioButton("First and third person", &camaraSelected, 4))
		this->setCameras({ CameraType::FirstPerson, CameraType::ThirdPerson });
	if(ImGui::RadioButton("Race, free and third person", &camaraSelected, 5))
		this->setCameras({ CameraType::Race, CameraType::Free, CameraType::ThirdPerson });
	if(ImGui::RadioButton("Third person and free", &camaraSelected, 6))
		this->setCameras({ CameraType::ThirdPerson, CameraType::Free });


	ImGui::End();
}
void InterfaceImGui::renderIntoScreen()
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void InterfaceImGui::destroy()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}



void InterfaceImGui::updateSpeed(float newSpeed)
{
	m_speed = newSpeed;
}

void InterfaceImGui::setTrihedron(bool split)
{
	m_trihedron = false;
}

bool InterfaceImGui::getTrihedron()
{
	return m_trihedron;
}

void InterfaceImGui::speedDown(float up)
{
	int nextSpeed = interface_speed - up;
	interface_speed = (nextSpeed >= SPEED_MAX) ? interface_speed - up : SPEED_MAX;
}

void InterfaceImGui::speedUp(float down)
{
	int nextSpeed = interface_speed + down;
	interface_speed = (nextSpeed <= SPEED_MIN) ? interface_speed + down : SPEED_MIN;
}
