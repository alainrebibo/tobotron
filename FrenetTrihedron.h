#ifndef FRENETTRIHEDRON_CLASS_H
#define FRENETTRIHEDRON_CLASS_H

#include "Utils.h"
#include "shaderClass.h"
#include "Camera.h"

class FrenetTrihedron
{
public:
	FrenetTrihedron(GLfloat t, Point3D* ctr);
	void Draw();
	Point3D Pos;
	Point3D TV;
	Point3D PNV;
	Point3D BNV;

private:
	void calculateTV(GLfloat t, Point3D* ctr);
	void calculateBNV(GLfloat t, Point3D* ctr);
	void calculatePNV();
};
#endif