#include"Model.h"

Model::Model(const char* filename, unsigned int instances, std::vector<glm::mat4> instanceMatrices) {
	Model::instances = instances;
	Model::instanceMatrices = instanceMatrices;
	loadModel(filename);
}

void Model::Draw(Shader& shader, Camera& camera, glm::mat4 transformation, Texture* texture) {
	for (unsigned int i = 0; i < meshes.size(); i++) {
		meshes[i].Mesh::Draw(shader, camera, transformation, texture);
	}
}

bool StartWith(std::string& line, const char* text) {
	size_t textLen = strlen(text);
	if (line.size() < textLen) {
		return false;
	}
	for (size_t i = 0; i < textLen; i++) {
		if (line[i] == text[i]) continue;
		else return false;
	}
	return true;
}

int Model::loadModel(const char* filename) {

	std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
	std::vector< glm::vec3 > temp_vertices;
	std::vector< glm::vec2 > temp_uvs;
	std::vector< glm::vec3 > temp_normals;

	std::vector < glm::vec3 > out_vertices;
	std::vector < glm::vec2 > out_uvs;
	std::vector < glm::vec3 > out_normals;
	std::vector < std::pair<std::string, unsigned int>> textureIndices;

	std::vector<Vertex> vertices;
	std::vector<Texture> textures;
	std::vector<std::pair<std::string, glm::vec3>> colors;

	std::ifstream file(filename);
	if (not file) {
		std::cout << "Obj file loading failed" << std::endl;
		return -1;
	}

	char textureName[256];
	std::string line;
	while (std::getline(file, line)) {
		//MATERIALS
		if (StartWith(line, "mtllib")) {
			char mtlFileName[100];
			sscanf_s(line.c_str(), "mtllib %s", mtlFileName, (unsigned int) sizeof(mtlFileName));
			std::string stringFileName = std::string(filename);
			size_t pos = stringFileName.find_last_of('/');
			std::string completeMtlFileName = stringFileName.substr(0, pos) + '/' + std::string(mtlFileName);

			loadMaterials(completeMtlFileName.c_str(), &textures, &colors);
		}
		//COORDS
		else if (StartWith(line, "v ")) {
			glm::vec3 vertex;
			sscanf_s(line.c_str(), "v %f %f %f", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		//UV TEXTURAS
		else if (StartWith(line, "vt ")) {
			glm::vec2 uv;
			sscanf_s(line.c_str(), "vt %f %f", &uv.x, &uv.y);
			temp_uvs.push_back(uv);
		}
		//NORMALES
		else if (StartWith(line, "vn ")) {
			glm::vec3 normal;
			sscanf_s(line.c_str(), "vn %f %f %f", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (StartWith(line, "usemtl ")) {
			sscanf_s(line.c_str(), "usemtl %s", textureName, (unsigned int)sizeof(textureName));
			textureIndices.push_back({ textureName, (unsigned int)vertexIndices.size() });
		}
		//CARAS
		else if (StartWith(line, "f ")) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			std::size_t found = line.find("//");
			if (found != std::string::npos) {
				sscanf_s(line.c_str(), "f %d//%d %d//%d %d//%d\n", &vertexIndex[0], &normalIndex[0], &vertexIndex[1], &normalIndex[1], &vertexIndex[2], &normalIndex[2]);
			}
			else {
				sscanf_s(line.c_str(), "f %d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
				uvIndices.push_back(uvIndex[0]);
				uvIndices.push_back(uvIndex[1]);
				uvIndices.push_back(uvIndex[2]);
			}

			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
	}

	for (unsigned int i = 0; i < textureIndices.size(); i++) {
		unsigned int initialNumber = textureIndices[i].second;
		unsigned int lastNumber = i == textureIndices.size() - 1? vertexIndices.size() : textureIndices[i + 1].second;
		// For each vertex of each triangle
		for (unsigned int j = initialNumber; j < lastNumber; j++) {
			unsigned int vertexIndex = vertexIndices[j];
			glm::vec3 vertex = temp_vertices[vertexIndex - 1];
			out_vertices.push_back(vertex);
		}
		for (unsigned int j = initialNumber; j < lastNumber; j++) {
			unsigned int uvIndex = uvIndices[j];
			glm::vec2 uv = temp_uvs[uvIndex - 1];
			out_uvs.push_back(uv);
		}
		for (unsigned int j = initialNumber; j < lastNumber; j++) {
			unsigned int normalIndex = normalIndices[j];
			glm::vec3 normal = temp_normals[normalIndex - 1];
			out_normals.push_back(normal);
		}

		unsigned int textureIndex = 0;
		for (textureIndex = 0; textureIndex < textures.size(); textureIndex++) {
			if (textures[textureIndex].name == textureIndices[i].first)
				break;
		}

		// Combine all the vertex components and also get the indices and textures
		vertices = assembleVertices(out_vertices, out_normals, out_uvs, colors[i].second);
		std::vector <Texture> mesh_textures = 
			textureIndex == textures.size() ? 
			std::vector <Texture>{} :
			std::vector <Texture>{ textures[textureIndex] };
			
		meshes.push_back(Mesh(vertices, mesh_textures, instances, instanceMatrices));
		out_vertices.clear();
		out_normals.clear();
		out_uvs.clear();
	}
}

int Model::loadMaterials
(
	const char* filename,
	std::vector<Texture>* textures,
	std::vector<std::pair<std::string, glm::vec3>>* out_colors)
{
	

	std::ifstream file(filename);
	if (not file) {
		std::cout << "MTL file loading failed" << std::endl;
		return -1;
	}

	std::string line;
	std::vector<glm::vec3> colors = {};
	std::vector<std::string> textureNames = {};
	std::vector<std::string> textureFileNames = {};
	while (std::getline(file, line))
	{
		if (StartWith(line, "newmtl"))
		{
			char textureNameC[256];
			sscanf_s(line.c_str(), "newmtl %s", textureNameC, (unsigned int)sizeof(textureNameC));
			textureNames.push_back(std::string(textureNameC));

			if (textureFileNames.size() < colors.size()) {
				textureFileNames.push_back("");
			}
		}
		else if (StartWith(line, "Kd"))
		{
			glm::vec3 color;
			sscanf_s(line.c_str(), "Kd %f %f %f", &color.r, &color.g, &color.b);
			colors.push_back(color);
		}
		else if (StartWith(line, "map_Kd")) {
			char textureFileName[256];
			sscanf_s(line.c_str(), "map_Kd %s", textureFileName, (unsigned int) sizeof(textureFileName));
			std::string stringFileName = std::string(filename);
			size_t pos = stringFileName.find_last_of('/');
			std::string completeTextureFileName = stringFileName.substr(0, pos) + '/' + std::string(textureFileName);
			textureFileNames.push_back(completeTextureFileName);
		}
	}

	for (unsigned int i = 0; i < textureNames.size(); i++) {
		if (textureFileNames[i] != "") {
			Texture texture = Texture(textureNames[i], textureFileNames[i].c_str(), "diffuse", 0);
			textures->push_back(texture);
		}
		out_colors->push_back({ textureNames[i], colors[i] });
	}

	return 0;
}

std::vector<Vertex> Model::assembleVertices
(
	std::vector<glm::vec3> positions,
	std::vector<glm::vec3> normals,
	std::vector<glm::vec2> texUVs,
	glm::vec3 color
) 
{
	std::vector<Vertex> vertices;
	for (int i = 0; i < positions.size(); i++)
	{
		vertices.push_back
		(
			Vertex
			{
				positions[i], normals[i], color, texUVs[i]
			}
		);   
	}
	return vertices;
}