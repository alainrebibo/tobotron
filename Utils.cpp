#include "Utils.h"

Point3D Prod_Vectorial(Point3D v1, Point3D v2)
{
	GLdouble longitud;
	Point3D vn;

	vn.x = v1.y * v2.z - v2.y * v1.z;
	vn.y = v1.z * v2.x - v2.z * v1.x;
	vn.z = v1.x * v2.y - v1.y * v2.x;

	// Convertim el vector en vector unitat (normalitzaci�)
	longitud = sqrt(vn.x * vn.x + vn.y * vn.y + vn.z * vn.z);
	vn.x = vn.x / longitud;
	vn.y = vn.y / longitud;
	vn.z = vn.z / longitud;

	return vn;
}


Point3D Punt_Corba_BSpline(GLfloat t, Point3D* ctr)
{
	Point3D p = { (0, 0, 0) };
	GLdouble coef[4];
	GLint i, j;

	// Polinomis que multipliquen els punts de control del patch
	for (i = 0; i < 4; i++)
	{
		coef[i] = 0;
		for (j = 0; j < 4; j++)
			coef[i] = coef[i] * t + AS[i][j];
	}

	// C�lcul de la Posici�
	for (i = 0; i < 4; i++)
	{
		p.x += coef[i] * ctr[i].x;
		p.y += coef[i] * ctr[i].y;
		p.z += coef[i] * ctr[i].z;
	}
	return p;

}

Point3D D_BSpline_Curve(GLfloat t, Point3D* ctr)
{
	Point3D dp = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLdouble coef[4];
	GLint i, j;


	// Polinomis que multipliquen els punts de control del patch
	for (i = 0; i < 4; i++)
	{
		coef[i] = 0;
		for (j = 0; j < 3; j++)
			coef[i] = coef[i] * t + (3 - j) * AS[i][j];
	}

	// C�lcul de la Primera Derivada
	for (i = 0; i < 4; i++)
	{
		dp.x += coef[i] * ctr[i].x;
		dp.y += coef[i] * ctr[i].y;
		dp.z += coef[i] * ctr[i].z;
	}
	return dp;
}

Point3D D2_BSpline_Curve(GLfloat t, Point3D* ctr)
{
	Point3D dp = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLdouble coef[4];
	GLint i, j;

	// Polinomis que multipliquen els punts de control del patch
	for (i = 0; i < 4; i++)
	{
		coef[i] = 0;
		for (j = 0; j < 2; j++)
			coef[i] = coef[i] * t + (3 - j) * (2 - j) * AS[i][j];
	}

	// C�lcul de la Segona Derivada
	for (i = 0; i < 4; i++)
	{
		dp.x += coef[i] * ctr[i].x;
		dp.y += coef[i] * ctr[i].y;
		dp.z += coef[i] * ctr[i].z;
	}
	return dp;
}