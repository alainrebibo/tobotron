#include"Mesh.h"
#include <map>

class Model {
public:
	Model(const char* filename, unsigned int instances = 1, std::vector<glm::mat4> instanceMatrices = {});
	void Draw
	(
		Shader& shader,
		Camera& camera,
		glm::mat4 transformation = glm::mat4(1.0f),
		Texture* texture = nullptr
	);

private:
	int loadModel(const char* filename);

	std::vector<Vertex> assembleVertices
	(
		std::vector<glm::vec3> positions,
		std::vector<glm::vec3> normals,
		std::vector<glm::vec2> texUVs,
		glm::vec3 color
	);
	int loadMaterials
	(
		const char* filename,
		std::vector<Texture>* textures,
		std::vector<std::pair<std::string, glm::vec3>>* out_colors
	);

	std::vector<Mesh> meshes;
	unsigned int instances;
	std::vector<glm::mat4> instanceMatrices;
};