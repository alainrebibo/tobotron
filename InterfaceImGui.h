#ifndef INTERFACE_CLASS_H
#define INTERFACE_CLASS_H
#include <string>
#include <GLFW/glfw3.h>
#include <vector>
#include <algorithm>

enum class CameraType { Race, Free, FirstPerson, ThirdPerson };
enum class LandscapeType { Normal, Desert, Volcano, Alpine };


class InterfaceImGui
{
private:
	LandscapeType m_landscape[4] = { LandscapeType::Normal, LandscapeType::Desert, LandscapeType::Volcano, LandscapeType::Alpine };
	const char* m_landscape_choices[4] = { "Snow", "Desert", "Volcano", "Alpine" };
	float interface_speed;
	float m_speed;
	int m_currentLandscape;
	bool m_trihedron;
	bool firstF3 = true;
	bool firstF4 = true;
	bool firstF5 = true;
	bool firstF6 = true;
	bool freeModeOn = false;
	bool firstPersonOn = false;
	std::vector<CameraType> cameraTypes;
	const float SPEED_MAX = 0;
	const float USER_SPEED_MIN = 0.05f;
	const float SPEED_MIN = 100;
	int camaraSelected = 2;

	void renderElements();
	void renderIntoScreen();

public:
	InterfaceImGui();

	//int setUp(GLFWwindow* window);
	void startNewFrame();
	void render();
	void destroy();

	void Inputs(GLFWwindow* window);

	void updateSpeed(float newSpeed);
	void setTrihedron(bool split);
	bool getTrihedron();
	void speedDown(float up);
	void speedUp(float down);
	float getSpeed() { return m_speed; }
	LandscapeType getLandscapeType() { return m_landscape[m_currentLandscape]; }
	std::vector<CameraType> getCameraTypes() { return cameraTypes; }
	void setCameras(std::vector<CameraType> cameras);

};

#endif INTERFACE_CLASS_H
