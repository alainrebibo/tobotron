#include "FrenetTrihedron.h"
#include <vector>

void changeToYup(Point3D* point) {
	GLdouble aux = point->y;
	point->x = point->x;
	point->y = point->z;
	point->z = -aux;
}

FrenetTrihedron::FrenetTrihedron(GLfloat t, Point3D* ctr)
{
	if (t >= 1.0)
	{
		FrenetTrihedron::Pos = Punt_Corba_BSpline(1.0, ctr);
	}
	else
	{
		FrenetTrihedron::Pos = Punt_Corba_BSpline(t, ctr);
	}

	FrenetTrihedron::calculateTV(t, ctr);
	FrenetTrihedron::calculateBNV(t, ctr);
	FrenetTrihedron::calculatePNV();

	changeToYup(&Pos);
	changeToYup(&TV);
	changeToYup(&BNV);
	changeToYup(&PNV);
}

void FrenetTrihedron::calculateTV(GLfloat t, Point3D* ctr)
{
	Point3D vt = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLdouble longitut = 0;

	vt = D_BSpline_Curve(t, ctr);

	// Convertim el vector en vector unitat (normalitzaciķ)
	longitut = sqrt(vt.x * vt.x + vt.y * vt.y + vt.z * vt.z);
	vt.x = vt.x / longitut;
	vt.y = vt.y / longitut;
	vt.z = vt.z / longitut;
	
	FrenetTrihedron::TV = vt;
}

void FrenetTrihedron::calculateBNV(GLfloat t, Point3D* ctr)
{
	
	Point3D vt1 = { 0.0f, 0.0f, 0.0f, 1.0f };
	Point3D vt2 = { 0.0f, 0.0f, 0.0f, 1.0f };
	Point3D vbn = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat longitut = 0;

	vt1 = D_BSpline_Curve(t, ctr);
	vt2 = D2_BSpline_Curve(t, ctr);

	vbn = Prod_Vectorial(vt1, vt2);

	FrenetTrihedron::BNV = { 0.0,0.0,1.0,0.0 };
}

void FrenetTrihedron::calculatePNV()
{
	Point3D vnp = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLdouble longitut = 0;

	vnp = Prod_Vectorial(FrenetTrihedron::BNV, FrenetTrihedron::TV);

	// Convertim el vector en vector unitat (normalitzaciķ)
	longitut = sqrt(vnp.x * vnp.x + vnp.y * vnp.y + vnp.z * vnp.z);
	vnp.x = vnp.x / longitut;
	vnp.y = vnp.y / longitut;
	vnp.z = vnp.z / longitut;

	FrenetTrihedron::PNV = vnp;
}

void draw_GL_LINES_VAO(std::vector <double> vertices, std::vector <double> colors)
{
	// ----------------------- VAO, VBO
	GLuint vaoId = 0, vboId = 0;

	// Create Vertex Array Object (VAO) for 3D Model Cube
	glGenVertexArrays(1, &vaoId);

	// Create vertex buffer objects for 3D Model attributes in the VAO
	glGenBuffers(1, &vboId);

	// Bind our Vertex Array Object as the current used object
	glBindVertexArray(vaoId);

	// Bind our Vertex Buffer Object as the current used object
	glBindBuffer(GL_ARRAY_BUFFER, vboId);

	glBufferData(GL_ARRAY_BUFFER, (vertices.size() + colors.size()) * sizeof(double), 0, GL_STATIC_DRAW);	// Allocate data to VBO starting from 0 offest

	// Position Vertex attributes
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(double), &vertices[0]);	// Copy geometry data to VBO starting from 0 offest
	glEnableVertexAttribArray(0);											// Enable attribute index 0 as being used (position)
	glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 3 * sizeof(double), 0);	// Specify that our coordinate data is going into attribute index 0 and contains 3 double

	// Color Vertex Attributes
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(double), colors.size() * sizeof(double), &colors[0]);	// Copy normal data to VBO starting from 0 offest
	glEnableVertexAttribArray(1);										// Enable attribute index 1 as being used (normals)
	glVertexAttribPointer(1, 4, GL_DOUBLE, GL_FALSE, 4 * sizeof(double), (GLvoid*)(vertices.size() * sizeof(double)));	// Specify that our color data is going into attribute index 0 and contains 3 double

	//Unbind the registered VBO as current VBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, (int) vertices.size() / 3);

	// Dissable Attrib arrays
	glDisableVertexAttribArray(0);		// Disable attribute index 0 as being used (Position)
	glDisableVertexAttribArray(1);		// Disable attribute index 1 as being used (Color)

	// It is good idea to release VBOs with ID 0 after use.
	// Once bound with 0, all pointers in gl*Pointer() behave as real
	// pointer, so, normal vertex array operations are re-activated
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vboId);

	// Unbind and delete VAO
	glBindVertexArray(0);
	glDeleteVertexArrays(1, &vaoId);
}

void FrenetTrihedron::Draw() {
	const int incr = 10;
	double color_objecte[4];
	bool sw_mat[4] = { false, true, true, false };

	// Doblar el gruix de la linia dels eixos.
	glLineWidth(2.0);

	// VAO
	std::vector <double> vertices, colors;		// Definiciķ vectors dināmics per a vertexs i colors 
	vertices.clear();		colors.clear();	// Reinicialitzar vectors

	// Eix VBN (vermell)
	color_objecte[0] = 1.0;	color_objecte[1] = 0.0; color_objecte[2] = 0.0;	color_objecte[3] = 1.0;

	colors.push_back(1.0);		colors.push_back(0.0);		colors.push_back(0.0);		colors.push_back(1.0);
	vertices.push_back(Pos.x);	vertices.push_back(Pos.y);	vertices.push_back(Pos.z);

	colors.push_back(1.0);		colors.push_back(0.0);		colors.push_back(0.0);		colors.push_back(1.0);
	vertices.push_back(Pos.x + incr * BNV.x);	vertices.push_back(Pos.y + incr * BNV.y);	vertices.push_back(Pos.z + incr * BNV.z);

	draw_GL_LINES_VAO(vertices, colors);
	vertices.clear();	colors.clear();		// Reinicialitzar vectors

	// Eix VNP (verd)
	color_objecte[0] = 0.0;	color_objecte[1] = 1.0; color_objecte[2] = 0.0;	color_objecte[3] = 1.0;

	colors.push_back(0.0);		colors.push_back(1.0);		colors.push_back(0.0);		colors.push_back(1.0);
	vertices.push_back(Pos.x);	vertices.push_back(Pos.y);	vertices.push_back(Pos.z);

	colors.push_back(0.0);		colors.push_back(1.0);		colors.push_back(0.0);		colors.push_back(1.0);
	vertices.push_back(Pos.x + incr * PNV.x);	vertices.push_back(Pos.y + incr * PNV.y);	vertices.push_back(Pos.z + incr * PNV.z);

	draw_GL_LINES_VAO(vertices, colors);
	vertices.clear();			colors.clear();		// Reinicialitzar vectors

	// Eix VT (blau) 
	color_objecte[0] = 0.0;	color_objecte[1] = 1.0;	color_objecte[2] = 1.0;	color_objecte[3] = 1.0;
	colors.push_back(0.0);			colors.push_back(1.0);			colors.push_back(1.0);		colors.push_back(1.0);
	vertices.push_back(Pos.x);	vertices.push_back(Pos.y);	vertices.push_back(Pos.z);

	colors.push_back(0.0);			colors.push_back(1.0);			colors.push_back(1.0);		colors.push_back(1.0);
	vertices.push_back(Pos.x + incr * TV.x);	vertices.push_back(Pos.y + incr * TV.y);	vertices.push_back(Pos.z + incr * TV.z);

	draw_GL_LINES_VAO(vertices, colors);

	// Restaurar el gruix de la linia dels eixos
	glLineWidth(1.0);
}

