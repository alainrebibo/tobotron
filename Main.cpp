#include "Model.h"
#include "Curve.h"
#include "Skybox.h"
#include "InterfaceImGui.h"

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>
#include "Main.h"

int width = 1440;
int height = 720;

int WinMain()
{
	//Instance ImGUI
	InterfaceImGui ui;
	// Initialize GLFW
	glfwInit();

	// Tell GLFW what version of OpenGL we are using 
	// In this case we are using OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Tell GLFW we are using the CORE profile
	// So that means we only have the modern functions
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Create a GLFWwindow object of 1440 by 720 pixels, naming it "Tobotronc"
	GLFWwindow* window = glfwCreateWindow(width, height, "Tobotronc", NULL, NULL);
	// Error check if the window fails to create
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	// Introduce the window into the current context
	glfwMakeContextCurrent(window);

	//Load GLAD so it configures OpenGL
	gladLoadGL();

	// Set the frame icon
	GLFWimage images[1];
	images[0].pixels = stbi_load("Resources/icon_small.png", &images[0].width, &images[0].height, 0, 4); //rgba channels 
	glfwSetWindowIcon(window, 1, images);
	stbi_image_free(images[0].pixels);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	// Setup Platform/Renderer bindings
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 330");
	// Setup Dear ImGui style
	ImGui::StyleColorsDark();


	// Generates Shader objects
	Shader shaderProgram("Resources/Shaders/default.vert", "Resources/Shaders/default.frag");
	Shader trihedronsShader("Resources/Shaders/trihedrons.vert", "Resources/Shaders/trihedrons.frag");
	Shader trackShader("Resources/Shaders/track.vert", "Resources/Shaders/default.frag");
	Shader depthMapShader("Resources/Shaders/shadowMap.vert", "Resources/Shaders/shadowMap.frag");

	glm::vec3 lightPos(997.379517, 1669.77576, -2904.90649);
	shaderProgram.Activate();
	glUniform3f(glGetUniformLocation(shaderProgram.ID, "lightPos"), lightPos.x, lightPos.y, lightPos.z);
	trackShader.Activate();
	glUniform3f(glGetUniformLocation(trackShader.ID, "lightPos"), lightPos.x, lightPos.y, lightPos.z);

	// Create camera object
	Camera firstPersonCamera(width / 2, height, glm::vec3(0.0f, 0.0f, 2.0f));
	Camera thirdPersonCamera(width / 2, height, glm::vec3(0.0f, 0.0f, 2.0f));
	Camera raceCamera(width / 2, height, glm::vec3(0.0f, 0.0f, 2.0f));
	Camera freeCamera(width / 2, height, glm::vec3(322.311829f, 334.394012f, -529.415588f));

	glm::vec3 cinemaCameras[] =
	{
		glm::vec3(-4.42524242f, 229.675674f, -92.4932022f),
		glm::vec3(-116.244560f, 201.324448f, -218.331833f),
		glm::vec3(-532.417236f, 161.696548f, -342.837616f)
	};

	// Load the track
	Curve tobotronc = Curve("Resources/Curves/Tobotron2.crv", 0.012f);
	std::vector<FrenetTrihedron> frenetTrihedrons = tobotronc.getFrenetTrihedrons();

	// Calcualte the base changes to be used for the camera and the different pieces of the track
	std::vector<glm::mat4> baseChanges;
	std::vector<glm::mat4> instanceMatrices;
	for (unsigned int i = 0; i < frenetTrihedrons.size(); i++)
	{
		FrenetTrihedron actual = frenetTrihedrons[i];
		glm::mat4 baseChange = glm::mat4(
			{ actual.PNV.x,	actual.PNV.y,	actual.PNV.z,	0 },
			{ actual.BNV.x, actual.BNV.y,	actual.BNV.z,	0 },
			{ actual.TV.x,	actual.TV.y,	actual.TV.z,	0 },
			{ 0,            0,             0,				1 }
		);
		baseChanges.push_back(baseChange);

		glm::mat4 trans = glm::mat4(1.0f);
		trans = glm::translate(trans, glm::vec3(actual.Pos.x, actual.Pos.y, actual.Pos.z));
		instanceMatrices.push_back(trans * baseChange);
	}

	// Load the models
	Model track("Resources/Models/track/via.obj", instanceMatrices.size(), instanceMatrices);
	Model wagon("Resources/Models/wagon/thomas.obj");

	// Define the positon of the 4 landscapes to be loaded to represent a valley
	glm::mat4 transformationLandscape = glm::mat4(1.0f);
	transformationLandscape = glm::rotate(transformationLandscape, glm::radians(-7.0f), { 0.0f, 1.0f, 0.0f });
	transformationLandscape = glm::translate(transformationLandscape, { -200.0f, 1.0f, 200.0f });
	transformationLandscape = glm::scale(transformationLandscape, glm::vec3(60.0f));
	
	glm::mat4 transformationLandscape1 = glm::mat4(1.0f);
	transformationLandscape1 = glm::rotate(transformationLandscape1, glm::radians(-187.0f), { 0.0f, 1.0f, 0.0f });
	transformationLandscape1 = glm::translate(transformationLandscape1, { 1400.0f, 1.0f, 1000.0f });
	transformationLandscape1 = glm::scale(transformationLandscape1, glm::vec3(60.0f));
	
	glm::mat4 transformationLandscape2 = glm::mat4(1.0f);
	transformationLandscape2 = glm::rotate(transformationLandscape2, glm::radians(-97.0f), { 0.0f, 1.0f, 0.0f });
	transformationLandscape2 = glm::translate(transformationLandscape2, { 200.0f, 1.0f, 1400.0f });
	transformationLandscape2 = glm::scale(transformationLandscape2, glm::vec3(60.0f));
	
	glm::mat4 transformationLandscape3 = glm::mat4(1.0f);
	transformationLandscape3 = glm::rotate(transformationLandscape3, glm::radians(-277.0f), { 0.0f, 1.0f, 0.0f });
	transformationLandscape3 = glm::translate(transformationLandscape3, { 1000.0f, 1.0f, -200.0f });
	transformationLandscape3 = glm::scale(transformationLandscape3, glm::vec3(60.0f));

	// Load all the possible textures of the landscape
	Texture normal("normal", "Resources/Models/landscape/landscape.jpg", "diffuse", 0);
	Texture desert("desert", "Resources/Models/landscape/desert.jpg", "diffuse", 0);
	Texture volcano("volcano", "Resources/Models/landscape/volcano.jpg", "diffuse", 0);
	Texture alpine("alpine", "Resources/Models/landscape/alpine.jpg", "diffuse", 0);
	std::vector<Texture> landscapeTextures = { normal, desert, volcano, alpine };
	
	// Load the model using instancing to get 4 mountains one next to another forming the valley
	Model landscape("Resources/Models/landscape/landscape.obj", 4, 
		{ transformationLandscape, transformationLandscape1, transformationLandscape2, transformationLandscape3 });

	// Load the skybox
	Skybox skybox = Skybox(width, height);

	// Enables the Depth Buffer
	glEnable(GL_DEPTH_TEST);

	std::cout << glGetString(GL_VERSION) << std::endl;

	// Framebuffer for Shadow Map
	unsigned int shadowMapFBO;
	glGenFramebuffers(1, &shadowMapFBO);

	// Texture for Shadow Map FBO
	unsigned int shadowMapWidth = 4096, shadowMapHeight = 4096;
	unsigned int shadowMap;
	glGenTextures(1, &shadowMap);
	glBindTexture(GL_TEXTURE_2D, shadowMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapWidth, shadowMapHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	// Prevents darkness outside the frustrum
	float clampColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, clampColor);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowMap, 0);
	// Needed since we don't touch the color buffer
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Matrices needed for the light's perspective
	glm::mat4 orthgonalProjection = glm::ortho(-2000.0f, 2000.0f, -2000.0f, 2000.0f, 0.1f, 5000.0f);
	glm::mat4 lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 lightSpaceMatrix = orthgonalProjection * lightView;

	depthMapShader.Activate();
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.ID, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

	double prevTime = glfwGetTime();

	GLint frenetIndex = 0;
	GLint cinemaCameraIndex = 0;
	FrenetTrihedron actualThriedron = frenetTrihedrons[frenetIndex];
	glm::vec3 actualCinemaCamera = cinemaCameras[cinemaCameraIndex];
	glm::vec3 translation = glm::vec3(0);
	Texture landscapeTexture;

	while (!glfwWindowShouldClose(window))
	{
		// Control when the user presses ESC to close the window
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) break;

		// Feed inputs to dear imgui, start new frame
		ui.startNewFrame();

		// Handles the user inputs
		ui.Inputs(window);

		// Calculate the different transformations to be applied to the pieces and camera
		double crntTime = glfwGetTime();
		if (crntTime - prevTime >= ui.getSpeed()) {
			frenetIndex = frenetIndex == frenetTrihedrons.size() - 1 ? 0 : frenetIndex + 1;
			cinemaCameraIndex = (int) frenetIndex / (frenetTrihedrons.size() / cinemaCameras->length());
			
			actualThriedron = frenetTrihedrons[frenetIndex];
			translation = { actualThriedron.Pos.x, actualThriedron.Pos.y,   actualThriedron.Pos.z };
			actualThriedron.Pos.y += 6.0f;

			prevTime = crntTime;
		}

		glm::mat4 transformationWagon = glm::mat4(1.0f);
		transformationWagon = glm::translate(transformationWagon, translation);
		transformationWagon = glm::scale(transformationWagon, { 0.25f, 0.25f, 0.25f });
		transformationWagon = transformationWagon * baseChanges[frenetIndex];
		
		// Draw the shadow map scene
		glEnable(GL_DEPTH_TEST);
		glViewport(0, 0, shadowMapWidth, shadowMapHeight);
		glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		// Draw scene for shadow map
		wagon.Draw(depthMapShader, freeCamera, transformationWagon);
		landscape.Draw(depthMapShader, freeCamera, transformationLandscape);
		track.Draw(depthMapShader, freeCamera);
		// Switch back to the default framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		
		glClearColor(1.0f,1.0f,1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Set the current landscape texture based on the interface selection
		landscapeTexture = landscapeTextures[int(ui.getLandscapeType())];

		std::vector<CameraType> cameraTypes = ui.getCameraTypes();
		int totalCameras = cameraTypes.size();
		for (int i = 0; i < totalCameras; i++) {
			float initialWidth = width * i / totalCameras;
			float cameraWidth = width / totalCameras;

			glViewport(initialWidth, 0, cameraWidth, height);
			// walk camera 
			if (cameraTypes[i] == CameraType::FirstPerson)
			{
				firstPersonCamera.updatePosition(actualThriedron.Pos, { 0, 0, 0 }, actualThriedron.BNV);
				firstPersonCamera.rotate(window);
				firstPersonCamera.setWidth(cameraWidth);
				drawScene(shaderProgram, trackShader,
					firstPersonCamera, shadowMap, landscapeTexture,
					lightSpaceMatrix, transformationWagon,
					wagon, landscape, track, skybox);
			}
			// camera free
			else if (cameraTypes[i] == CameraType::Free)
			{
				freeCamera.setWidth(cameraWidth);
				freeCamera.Inputs(window);
				if (ui.getTrihedron()) tobotronc.Draw(trihedronsShader, freeCamera);
				drawScene(shaderProgram, trackShader,
					freeCamera, shadowMap, landscapeTexture,
					lightSpaceMatrix, transformationWagon,
					wagon, landscape, track, skybox);
			}
			//
			else if (cameraTypes[i] == CameraType::Race)
			{
				raceCamera.updatePosition(actualThriedron.Pos, actualThriedron.TV, actualThriedron.BNV);
				raceCamera.setWidth(cameraWidth);
				drawScene(shaderProgram, trackShader,
					raceCamera, shadowMap, landscapeTexture,
					lightSpaceMatrix, transformationWagon,
					wagon, landscape, track, skybox);
			}
			else if (cameraTypes[i] == CameraType::ThirdPerson)
			{
				// Update the camera position and orientation
				actualCinemaCamera = cinemaCameras[cinemaCameraIndex];
				thirdPersonCamera.updatePosition(
					{ actualCinemaCamera.x, actualCinemaCamera.y, actualCinemaCamera.z },
					{ translation.x - actualCinemaCamera.x, translation.y - actualCinemaCamera.y, translation.z - actualCinemaCamera.z },
					{ 0, 1, 0 }
				);
				thirdPersonCamera.setWidth(cameraWidth);
				if (ui.getTrihedron()) tobotronc.Draw(trihedronsShader, thirdPersonCamera);
				drawScene(shaderProgram, trackShader,
					thirdPersonCamera, shadowMap, landscapeTexture,
					lightSpaceMatrix, transformationWagon,
					wagon, landscape, track, skybox);
			}
		}
		// render your GUI
		ui.render();
		// Swap the back buffer with the front buffer
		glfwSwapBuffers(window);
		// Take care of all GLFW events
		glfwPollEvents();
	}

	// Delete all the objects we've created
	shaderProgram.Delete();
	trihedronsShader.Delete();
	// Delete elements UI
	ui.destroy();
	// Delete window before ending the program
	glfwDestroyWindow(window);
	// Terminate GLFW before ending the program
	glfwTerminate();
	return 0;
}

void drawScene
(
	Shader& shader, Shader& instanceShader,
	Camera& camera, unsigned int shadowMap, Texture& landscapeTexture,
	glm::mat4& lightSpaceMatrix, glm::mat4& transformationWagon,
	Model& wagon, Model& landscape, Model& track, Skybox& skybox
)
{
	skybox.Draw(camera);
	// Draw the normal scene
	shader.Activate();
	glUniformMatrix4fv(glGetUniformLocation(shader.ID, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, shadowMap);
	glUniform1i(glGetUniformLocation(shader.ID, "shadowMap"), 1);
	wagon.Draw(shader, camera, transformationWagon);
	instanceShader.Activate();
	glUniformMatrix4fv(glGetUniformLocation(instanceShader.ID, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, shadowMap);
	glUniform1i(glGetUniformLocation(instanceShader.ID, "shadowMap"), 1);
	track.Draw(instanceShader, camera);
	landscape.Draw(instanceShader, camera, {}, &landscapeTexture);
}
