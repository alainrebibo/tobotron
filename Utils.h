#pragma once

#include <glad/glad.h>
#include <corecrt_math.h>

struct Point3D
{
	GLdouble x;
	GLdouble y;
	GLdouble z;
	GLdouble w;
};

const GLdouble AS[4][4] =
{
	{ -1.0 / 6.0, 0.5, -0.5, 1.0 / 6.0 },
	{ 0.5, -1.0, 0.0, 4.0 / 6.0 },
	{ -0.5, 0.5, 0.5, 1.0 / 6.0 },
	{ 1.0 / 6.0, 0.0, 0.0, 0.0 }
};

Point3D Prod_Vectorial(Point3D v1, Point3D v2);
Point3D D_BSpline_Curve(GLfloat t, Point3D* ctr);
Point3D D2_BSpline_Curve(GLfloat t, Point3D* ctr);
Point3D Punt_Corba_BSpline(GLfloat t, Point3D* ctr);
