#include "Curve.h"

Curve::Curve()
{
	const char *fileName = "Resources/Curves/Corba9 Hidro.crv";
	GLfloat frenetPeriod = 0.10f;

	saveFileContents(fileName);
	calculateFrenetThrihedrons(frenetPeriod);
}

Curve::Curve(const char* fileName, GLfloat frenetPeriod)
{
	saveFileContents(fileName);
	calculateFrenetThrihedrons(frenetPeriod);
}

std::vector<FrenetTrihedron> Curve::getFrenetTrihedrons()
{
	return Curve::frenetTrihedrons;
}

void Curve::saveFileContents(const char* fileName)
{
	/* Read the file specified on init and save the data in the class attributes */
	FILE* fd;
	std::vector<Point3D> controlPoints;
	int numPoints = 0;
	GLdouble x = 0.0, y = 0.0, z = 0.0;
	errno_t err;

	err = fopen_s(&fd, fileName, "rt");
	if (err) return;

	err = fscanf_s(fd, "%d \n", &numPoints);
	if (numPoints == 0) return;

	for (int i = 0; i < numPoints; i = i++)
	{
		err = fscanf_s(fd, "%lf %lf %lf \n", &x, &y, &z);

		controlPoints.push_back({ x, y, z });
	}
	
	fclose(fd);

	Curve::numPoints = numPoints;
	Curve::controlPoints = controlPoints;
}

void Curve::calculateFrenetThrihedrons(GLfloat period)
{
	Point3D ctr[4];
	int patch = 0;
	GLfloat t = 0;

	for (int i = 0; i < 4; i++)
	{
		ctr[i].x = Curve::controlPoints[i].x;
		ctr[i].y = Curve::controlPoints[i].y;
		ctr[i].z = Curve::controlPoints[i].z;
	}

	Curve::frenetTrihedrons.push_back(FrenetTrihedron(t, ctr));

	t = t + period;
	while (patch <= Curve::numPoints - 4) {
		if (t >= 1.0)
		{
			Curve::frenetTrihedrons.push_back(FrenetTrihedron(t, ctr));
			t = 0.0;
			patch++;
			if (patch <= Curve::numPoints - 4)
			{
				for (int i = 0; i < 4; i++)
				{
					ctr[i].x = Curve::controlPoints[patch + i].x;
					ctr[i].y = Curve::controlPoints[patch + i].y;
					ctr[i].z = Curve::controlPoints[patch + i].z;
				}
			}
		}
		else if (patch <= Curve::numPoints - 4) {
			Curve::frenetTrihedrons.push_back(FrenetTrihedron(t, ctr));
			t = t + period;
		}
	}
}

void Curve::Draw(Shader& shader, Camera &camera) {
	shader.Activate();
	glUniform3f(glGetUniformLocation(shader.ID, "camPos"), camera.Position.x, camera.Position.y, camera.Position.z);
	camera.Matrix(shader, "camMatrix");

	for (GLint i = 0; i < frenetTrihedrons.size(); i++) {
		frenetTrihedrons[i].Draw();
	}
}