#include "Mesh.h"

Mesh::Mesh
(
	std::vector <Vertex>& vertices,
	std::vector <Texture>& textures,
	unsigned int instances,
	std::vector <glm::mat4> instanceMatrices
)
{
	Mesh::vertices = vertices;
	Mesh::textures = textures;
	Mesh::instances = instances;

	VAO.Bind();
	// Generates Vertex Buffer Object and links it to vertices
	VBO instanceVBO(instanceMatrices);
	VBO VBO(vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	VAO.LinkAttrib(VBO, 0, 3, GL_FLOAT, sizeof(Vertex), (void*)0);
	VAO.LinkAttrib(VBO, 1, 3, GL_FLOAT, sizeof(Vertex), (void*)(3 * sizeof(float)));
	VAO.LinkAttrib(VBO, 2, 3, GL_FLOAT, sizeof(Vertex), (void*)(6 * sizeof(float)));
	VAO.LinkAttrib(VBO, 3, 2, GL_FLOAT, sizeof(Vertex), (void*)(9 * sizeof(float)));

	if (instances != 1)
	{
		instanceVBO.Bind();

		VAO.LinkAttrib(instanceVBO, 4, 4, GL_FLOAT, sizeof(glm::mat4), (void*)0);
		VAO.LinkAttrib(instanceVBO, 5, 4, GL_FLOAT, sizeof(glm::mat4), (void*)(1 * sizeof(glm::vec4)));
		VAO.LinkAttrib(instanceVBO, 6, 4, GL_FLOAT, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
		VAO.LinkAttrib(instanceVBO, 7, 4, GL_FLOAT, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);
		glVertexAttribDivisor(7, 1);
	}

	// Unbind all to prevent accidentally modifying them
	VAO.Unbind();
	VBO.Unbind();
	instanceVBO.Unbind();
}

void Mesh::Draw
(
	Shader& shader,
	Camera& camera,
	glm::mat4 transformation,
	Texture* texture
)
{
	// Bind shader to be able to access uniforms
	shader.Activate();
	VAO.Bind();

	if (texture == nullptr)
	{
		textures[0].texUnit(shader, "diffuse0", textures[0].unit);
		textures[0].Bind();
	}
	else
	{
		texture->texUnit(shader, "diffuse0", texture->unit);
		texture->Bind();
	}

	// Tell the shader how many instaces will be drawn
	glUniform1i(glGetUniformLocation(shader.ID, "instances"), instances);
	// Take care of the camera Matrix
	glUniform3f(glGetUniformLocation(shader.ID, "camPos"), camera.Position.x, camera.Position.y, camera.Position.z);
	camera.Matrix(shader, "camMatrix");

	if (instances == 1)
	{
		// Initialize matrices
		glm::mat4 trans = glm::mat4(1.0f);
		glm::mat4 sca = glm::mat4(1.0f);

		// Push the matrices to the vertex shader
		glUniformMatrix4fv(glGetUniformLocation(shader.ID, "transformation"), 1, GL_FALSE, glm::value_ptr(transformation));

		// Draw the actual mesh
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());
	}
	else
	{
		glDrawArraysInstanced(GL_TRIANGLES, 0, vertices.size(), instances);
	}
	for (unsigned int i = 0; i < textures.size(); i++)
	{
		textures[i].Unbind();
	}
}