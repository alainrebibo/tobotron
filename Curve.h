#ifndef CURVE_CLASS_H
#define CURVE_CLASS_H

#include <vector>
#include <glm/glm.hpp>
#include "Utils.h"
#include "FrenetTrihedron.h"
#include "Camera.h"

class Curve
{
public:
	Curve();
	Curve(const char* fileName, GLfloat frenetPeriod);
	std::vector<FrenetTrihedron> getFrenetTrihedrons();
	void Draw(Shader& shader, Camera& camera);

private:
	GLint numPoints;
	std::vector<Point3D> controlPoints;
	std::vector<GLdouble> dataPoints;
	std::vector<GLdouble> colors;
	std::vector<FrenetTrihedron> frenetTrihedrons;

	void saveFileContents(const char* fileName);
	void calculateFrenetThrihedrons(GLfloat period);
};
#endif