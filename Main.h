#pragma once

void drawScene
(
	Shader& shader, Shader& instanceShader,
	Camera& camera, unsigned int shadowMap, Texture& landscapeTexture,
	glm::mat4& lightSpaceMatrix, glm::mat4& transformationWagon,
	Model& wagon, Model& landscape, Model& track, Skybox& skybox
);